---
title: "Forgotten Password in Phoenix"
date: 2018-10-28T11:25:00-04:00
draft: false
publishDate: 2018-10-28T11:25:00-04:00
---

So I wanted to share how I added a forgot password feature to my side project. The code you will have to modify based on your project.

#### Overview of the steps I took
1. Create Password Controller
1. :new function in password controller
1. :create function in password controller
1. :reset function in password controller
1. template for user to enter their email
1. create a token module to handle tokens
1. create function to create tokens using [Phoenix.Token](https://hexdocs.pm/phoenix/Phoenix.Token.html) in your token module
1. Add :reset to route
1. Edit your :create in password controller to pass the token with your reset path to your email handler.
1. When user visit link verify token and its within age in :reset function
1. If all is good sign them in so they can change their password

I'm not an expert so there could be issues with this, but it seems to work fine for me.  Doing any forgotten password opens up a security issue in that anyone with access to their email can login as them.  Beyond that issue I beleive its fairly secure since phoenix tokens are signed but not encrypted. Since we are only passing user_id encrypting isn't really need.

One of thing I really like about this method is it doesn't affect the database.


#### Example Code
```
def new(conn, _params) do
    changeset = User.changeset(%User{}, %{})
    render(conn, "new.html", changeset: changeset)
end

def create(conn, %{"user" => user_params}) do
    case Accounts.get_user_by_email(user_params["email"]) do
      %User{} = user ->
        reset_token = MyApp.Token.generate_account_token(user)
        reset_url = Routes.password_url(conn, :reset, token: reset_token)

        # send to your reset_url to user email
      _ ->
        ""
    end

    # send this message regardless so hackers can't use it to test emails
    conn
    |> put_flash(:info, "Reset token sent to user")
    |> redirect(to: "/")
  end

  def reset(conn, %{"token" => token}) do
    with {:ok, user_id} <- MyApp.Token.verify_password_reset_token(token),
      user <- Accounts.get_user!(user_id) do
        conn
        |> MyAppWeb.AuthController.login(user)
        |> redirect(to: Routes.user_path(conn, :edit, user))
    else
      _ ->
        conn
      |> put_flash(:info, "Invalid Token")
      |> redirect(to: "/")
    end
  end
```

**In MyApp.Token**

```
@salt "random_hex"

  def generate_account_token(%User{id: user_id}) do
    Phoenix.Token.sign(MyAppWeb.Endpoint, @account_verification_salt, user_id)
  end

  def verify_password_reset_token(token) do
    max_age = 300 # tokens that are older than a 5 mins should be invalid
    Phoenix.Token.verify(MyAppWeb.Endpoint, @account_verification_salt, token, max_age: max_age)
  end
```

**In router**
`get "/reset-password", PasswordController, :reset` to your routes